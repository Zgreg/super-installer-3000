# Batch Script for Software Installation
This batch script downloads and installs multiple software packages on a Windows system, including Steam, Epic Games, Parsec, Brave, Spotify, Discord, Visual Studio Code, Gforce Experience and VoiceMeeter.

## Usage
- Run the script on a Windows machine.
- The script will download the installation files of the mentioned software packages and install them.
- After the installation is complete, the script will delete the installation files.
## Packages installed
- Steam
- Epic Games
- Parsec
- Brave
- Spotify
- Discord
- Visual Studio Code
- Gforce Experience
- VoiceMeeter

## Technical Details
- The script uses the bitsadmin command to download the installation files with a high priority.
- The installation files are stored in the %userprofile%\Downloads directory.
- The script uses the start command to launch the installation files.
- The script displays a message during the installation of each software package.
- After the installation is complete, the script deletes the installation files.